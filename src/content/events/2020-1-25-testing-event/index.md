---
path: /events/testevent
date: "2020-01-1"
author: "abbyck"
title: "Test event"
cover: "./poster2.jpeg"
---

![Poster](./poster2.jpeg)

This is a test event

## Event Details

Date: 01/01/2020
Time: 10.00 AM
Venue: Anywhere
